/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __PETTYCASHLOGIC_HPP__
#define __PETTYCASHLOGIC_HPP__

#include <QObject>
#include <string>
#include "pettyCashTypes.hpp"
#include "dataManager.hpp"

// the controller

class CPettyCashLogic: public QObject
{
    Q_OBJECT
    
public:    
    CPettyCashLogic(CDataManager* dataManager);
    
    // reads the database and sets the gui to the correct initial state
    void doInitialisation();


public slots:
    void slotActVoucher(const SVoucherData&);
    void slotAllVoucherView();

signals: 
    void signalVoucherStored(const SPettyCashData&); 
    

private: // methods
    SPettyCashData getFormattedDataFromStorage();

private: //attributes
    // session attributes
    int m_numVouchersStored;
    std::string m_dateString;
    std::string m_cashier;
    std::string m_pettycashname;
    float m_cashposition; 
    
    // act voucher attributes
    EVoucherType m_voucherType;
    float m_amount;
    std::string m_purpose;
    std::string m_description;
    
    // data model
    CDataManager* m_dataManager;
    
    // currently, only controller handels the voucher date
    std::string m_voucherDateString;


};


#endif
