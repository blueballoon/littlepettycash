# README Littlepettycash #

Littlepettycash is a small and simple program to manage a little pettycash.

### Requirements ###
The program is developed using Qt 4.8, gcc 4.x and scons 2.3 using Dirk Baechles Qt4 scons extension (see https://bitbucket.org/dirkbaechle/scons_qt4/overview) on Linux 64bit (currently CentOS 6.6), but it can also be built on Linux 32bit or even on MS Windows when using qmake (but not that the qmake build is nolonger maintained execpt for the neccessary steps to make the translation files).


### How do I build? ###

Assuming scons and qt4 are set up correctly, just type scons in the main directory (containing the SConstruct file). The executeable is named littlepettycash_sc (sc for scons, to make it distinguishable from the qmake build).


### How do I use it? ###

The desired configuration has to be set up in the  littlepettycash.cfg file. If another config file has to be used, its path and name has to be passsed as command line argument when starting the applications.

In the config file, which is a simple XML file,  the following properties can be set:

* <settings> basic program settings
    * <pettycashname> the name of the pettycash
    * <csvdatabase> the name of the csv file containing the pettycash data (if no path is given, it is created/searchend in the actual working dir)
    * <language> *en* for english, *de* for german
    * <confirmationdialog> *yes* if a confirmation dialog shall appear before saving the actual voucher into the csv file, *no* otherwise
*  <cashierlist> List of available cashiers; at least one cashier has to be provided
    * <cashier> Name of a cashier for this petticash
* <purposelist> List of available income/expense purposes; at least on purpose has to be provided (usually, at least one income and one expense purpose should be provided)
    *  <purpose type="income"> text for describing an incoming purpose
    *  <purpose type="expense"> text for describing an expense purpose

An example configuration file is provided in the repository and in the download packages.