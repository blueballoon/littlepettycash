/*
   Copyright 2014 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VOUCHERLISTDIALOG_HPP__
#define __VOUCHERLISTDIALOG_HPP__

#include <QDialog>
//#include <string>
//#include <list>
//#include <map>
#include "pettyCashTypes.hpp"

class QTableWidget;
class QVBoxLayout;
class QHBoxLayout;

class CVoucherListDialog : public QDialog
{
	Q_OBJECT
public:
	CVoucherListDialog(const std::list<SStoredVoucherData>& storedVoucherData,
			           QWidget *parent = 0);

private:
    QTableWidget* m_voucherTable;
};


#endif
