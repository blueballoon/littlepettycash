/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QFile>
#include <QTextStream>
#include <QRegExp>
#include "dataManager.hpp"


//debug only #include <iostream>

CDataManager::CDataManager(const std::string& dataFileName):
    m_dataFileName(dataFileName),
    m_separator(';'),
    m_replacechar(',')
{

}


SStoredDataSummary CDataManager::getBriefStoredData() const
{
	// data for the summary
    int lastVoucherNr = 0;
    float cashposition = 0.0f;

	QString dataFileName(m_dataFileName.c_str());
    QFile dataFile(dataFileName);


    // open file for reading
    if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // do some error handling
    }
    else
    {
        QTextStream dataInput(&dataFile);

        while (!dataInput.atEnd())
        {
            QString actLine = dataInput.readLine();
            SStoredVoucherData voucherData;

            if(parseLine(actLine, voucherData))
            {
                // todo: also count parsed lines and check if its identical with actVoucherNr.
            	lastVoucherNr = voucherData.voucherNr;
            	cashposition += voucherData.amount;
            }
        }
        dataFile.close();
    }

    // todo check: cashposition may not be negative ??
    SStoredDataSummary storedDataSummary;
    storedDataSummary.numStoredVouchers = lastVoucherNr;
    storedDataSummary.cashposition = cashposition;
    return(storedDataSummary);
}


std::list<SStoredVoucherData>  CDataManager::getDetailedStoredData() const
{
	QString dataFileName(m_dataFileName.c_str());
    QFile dataFile(dataFileName);

    std::list<SStoredVoucherData> storedDataList;

    // open file for reading
    if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // do some error handling
    }
    else
    {
        QTextStream dataInput(&dataFile);

        while (!dataInput.atEnd())
        {
            QString actLine = dataInput.readLine();
            SStoredVoucherData voucherData;

            if(parseLine(actLine, voucherData))
            {
            	storedDataList.push_back(voucherData);
            }
        }
        dataFile.close();
    }

    return(storedDataList);
}


void CDataManager::appendVoucherData(int voucherNr, std::string dateString,
                                     const SVoucherData& voucherData)
{
    QString dataFileName(m_dataFileName.c_str());
    QFile dataFile(dataFileName);

    // open file for writing
    if (!dataFile.open(QIODevice::Append))
    {
        // do some error handling
    }
    else
    {
        // if file is empty, add column header
        if (dataFile.size() == 0)
        {
            QTextStream headerOutput(&dataFile);
            headerOutput << "voucherNr" << m_separator
                         << "date" << m_separator
                         << "cashier" << m_separator
                         << "purpose" << m_separator
                         << "description" << m_separator
                         << "income" << m_separator
                         << "expense" << endl;
        }


        QTextStream dataOutput(&dataFile);
        dataOutput.setRealNumberNotation(QTextStream::FixedNotation);
        dataOutput.setRealNumberPrecision(2);

        // replace separator chars with the predefined replacechar
        QString strippedDescription(voucherData.description.c_str());
        strippedDescription.replace(m_separator,m_replacechar);  //alternative: remove

        QString strippedPurpose(voucherData.purpose.c_str());
        strippedPurpose.replace(m_separator,m_replacechar);  //alternative: remove

        QString strippedCashier(voucherData.cashier.c_str());
        strippedCashier.replace(m_separator,m_replacechar);  //alternative: remove

        dataOutput << voucherNr << m_separator
                   << dateString.c_str() << m_separator
                   << strippedCashier << m_separator
                   << strippedPurpose << m_separator
                   << strippedDescription << m_separator;
        if (vouchertype_income == voucherData.vouchertype)
        {
            dataOutput << voucherData.amount << m_separator << "0.00";
        }
        else
        {
            dataOutput << "0.00" << m_separator << voucherData.amount;
        }
        dataOutput << endl;
        dataFile.close();
    }
}


QRegExp CDataManager::getCsvRegExp() const
{
    // build regexp with m_separator
    // eg with komma (,) - the regex looks like this:
    // ^(\\d+),([^,]*),([^,]*),([^,]*),([^,]*),(\\d+\\.\\d\\d),(\\d+\\.\\d\\d)$
	// cap: 1 , 2     , 3     , 4     , 5     , 6             , 7

    QString regExpCsvLineQString;
    QTextStream regExpCsvLineStream(&regExpCsvLineQString);
    regExpCsvLineStream << "^(\\d+)" << m_separator;   // caption 1
    regExpCsvLineStream << "([^" << m_separator << "]*)" << m_separator; // caption 2
    regExpCsvLineStream << "([^" << m_separator << "]*)" << m_separator; // caption 3
    regExpCsvLineStream << "([^" << m_separator << "]*)" << m_separator; // caption 4
    regExpCsvLineStream << "([^" << m_separator << "]*)" << m_separator; // caption 5
    regExpCsvLineStream << "(\\d+\\.\\d\\d)" << m_separator;  // caption 6
    regExpCsvLineStream << "(\\d+\\.\\d\\d)$";                // caption 7

//    std::cerr << "regex is <" << regExpCsvLineQString.toStdString() << ">" << std::endl;
    QRegExp regExpCsvLine(regExpCsvLineQString);
    return(regExpCsvLine);
}



bool CDataManager::parseLine(const QString& line, SStoredVoucherData& voucherData) const
{
    bool parseOk = false;

    QRegExp regExpCsvLine = getCsvRegExp();

    QString voucherNrQStr = "0";
    QString voucherDateStr = "unknown";
    QString cashierStr = "unknown";
    QString purposeStr = "unknown";
    QString descriptionStr = "unknown";
    QString incomeQStr = "0.0";
    QString expenseQStr = "0.0";

    if(regExpCsvLine.indexIn(line) != -1)
    {
        voucherNrQStr = regExpCsvLine.cap(1);
        voucherDateStr = regExpCsvLine.cap(2);
        cashierStr = regExpCsvLine.cap(3);
        purposeStr = regExpCsvLine.cap(4);
        descriptionStr = regExpCsvLine.cap(5);
        incomeQStr = regExpCsvLine.cap(6);
        expenseQStr = regExpCsvLine.cap(7);

        float income = incomeQStr.toFloat();  //@todo check bool ok values!
        float expense = expenseQStr.toFloat();

        voucherData.voucherNr = voucherNrQStr.toInt();
        voucherData.voucherDate = voucherDateStr.toStdString();
		voucherData.cashier = cashierStr.toStdString();
		voucherData.purpose = purposeStr.toStdString();
		voucherData.description = descriptionStr.toStdString();
		voucherData.amount = income - expense;

        parseOk = true;
    }
    // ignore all lines in csv that doesnt match, return false

     /* std::cout << "nr:" << voucherNr << " value:" << voucherValue <<
       " actline: <" << line.toStdString() << ">" << std::endl;
     */
    return(parseOk);
}





