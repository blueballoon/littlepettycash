/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CFGMANAGER_HPP__
#define	__CFGMANAGER_HPP__

#include <string>
#include <list>
#include "pettyCashTypes.hpp"

class QDomElement;

class CCfgManager
{
public:
    CCfgManager(const std::string& configFileName);

    std::string getPettycashname() { return m_pettycashname; }
    std::string getCSVdatabaseName() { return m_CSVdatabaseName; }
    std::string getLanguage() { return m_language; }
    bool isConfirmationDialogActive() { return m_confirmationDialogActive; }

    std::list<std::string> getCashierList() { return m_cashierList; }
    ///@todo return a list of pair <purpose,type>
    std::list<std::pair<std::string,EVoucherType> > getPurposeList() { return m_purposeList; }

private:
    bool readAndParseConfigfile(const std::string&);
    bool parseLittlepettycashconfigElement(const QDomElement& element);
    bool parseSettingsElement(const QDomElement& element);
    bool parseCashierlistElement(const QDomElement& element);
    bool parsePurposelistElement(const QDomElement& element);
    

private: // attributes
    std::string m_pettycashname;
    std::string m_CSVdatabaseName;
    std::string m_language;
    bool m_confirmationDialogActive;

    std::list<std::string> m_cashierList;
    std::list<std::pair<std::string,EVoucherType> > m_purposeList;
};

#endif	/* __CFGMANAGER_HPP__ */

