/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui>
#include <QMessageBox>
#include <string>
#include "voucherEditDialog.hpp"

//debug only #include <iostream>

// constructor
CVoucherEditDialog::CVoucherEditDialog(const std::string& pettyCashName,
                                       const std::list<std::string>& cashierList,
                                       const std::list<std::pair<std::string,EVoucherType> >& purposeList,
                                       bool confirmationDialogActive,
                                       QWidget* parent):
        m_confirmationDialogActive(confirmationDialogActive),
        QDialog(parent)
{
    // fill a temporary string-list fpr the purpose-pair-list
    // fill the purpose-type map member that is use for the combobox-radiogroup connection logic
    std::list<std::string> purposeTextList;
    std::list<std::pair<std::string,EVoucherType> >::const_iterator purposeListIter;
    for (purposeListIter = purposeList.begin();
         purposeListIter !=  purposeList.end();
         ++purposeListIter)
    {
        purposeTextList.push_back((*purposeListIter).first);
        m_purposeTypeMap.insert((*purposeListIter));
    }


    createWidgets(pettyCashName,cashierList,purposeTextList);
    layoutWidgets();
    setupConnections();
    
}



// public slots
void CVoucherEditDialog::slotSetVoucherToInputState(const SPettyCashData& pettycashData)
{
    // std::cout << "set voucher ready to next input" << std::endl;
    
    // receive and set actual pettycash status values 
    QString voucherNrQString;
    voucherNrQString.setNum(pettycashData.nextVoucherNr);
    m_voucherNrValue->setText(voucherNrQString);   
    
    QString numStoredVouchersQString;
    numStoredVouchersQString.setNum(pettycashData.numStoredVouchers);
    m_numVouchersValue->setText(numStoredVouchersQString);
    
    QString cashpositionQString;
    cashpositionQString.setNum(pettycashData.cashposition,'f',2);  
    m_cashpositionValue->setText(cashpositionQString);
    
    QString dateQString(pettycashData.dateString.c_str());
    m_dateValue->setText(dateQString);
    
    // reset the voucher input fileds (except cashier and kind)
    // m_purposeEdit->clear();
    m_descrEdit->clear();
    m_amountEdit->clear();
    
    m_purposeCombo->setFocus(Qt::OtherFocusReason);
}



// private slots
void CVoucherEditDialog::slotCheckVoucher()
{
    // std::cout << "voucher check activated" << std::endl;
    
    // perform some base checks to allow processing of voucher
    bool voucherOk=false;   // todo first two checks have to be made at XML parser
    if (m_cashierCombo->currentText().size() != 0 &&   // at least, cashier
        m_purposeCombo->currentText().size() != 0 &&   // and purpose
        m_amountEdit->text().size() != 0 )     // and amount have to be entered
    {
        voucherOk=true;
    }
    
    
    // if check was ok, emit confirmed-signal
    if (voucherOk)
    {
        // fill voucher data transport struct
        SVoucherData voucherData;
        voucherData.cashier = m_cashierCombo->currentText().toStdString();
        voucherData.purpose =m_purposeCombo->currentText().toStdString();
        voucherData.description = m_descrEdit->text().toStdString();
        voucherData.amount = m_amountEdit->text().toFloat();
        
        if (m_voucherTypeIncomeRadio->isChecked())
        {
            voucherData.vouchertype = vouchertype_income;
        }
        else
        {
            voucherData.vouchertype = vouchertype_expense;
        }    
        
        
        // if additional confirmation is configured (in cfg file), then ask the
        // user to confirm the voucher
        int confirmationResult=QMessageBox::Save;
        if (m_confirmationDialogActive)
        {
            confirmationResult=getMessageDialogResult(voucherData);
        }

        if (QMessageBox::Save == confirmationResult)
        {
            emit signalVoucherConfirmed(voucherData);
        }
    }
}



void CVoucherEditDialog::slotDiscardVoucher()
{
    // std::cout << "voucher discarded" << std::endl;

    // reset the voucher input fileds (except cashier and kind)
    // m_purposeEdit->clear();
    m_descrEdit->clear();
    m_amountEdit->clear();    

}


void CVoucherEditDialog::slotPreselectVouchertype(const QString& purposeText)
{
    EVoucherType preselectedVoucherType = m_purposeTypeMap[purposeText.toStdString()];
    if (vouchertype_expense == preselectedVoucherType)
    {
        m_voucherTypeExpenseRadio->setChecked(true);
    }
    else
    {
        m_voucherTypeIncomeRadio->setChecked(true);
    }
}

// private helper functions
void CVoucherEditDialog::createWidgets(const std::string &pettyCashName,
                                       const std::list<std::string>& cashierList,
                                       const std::list<std::string>& purposeList)
{
	// group 1
    m_voucherInputGroup = new QGroupBox(tr("Voucher input"));
    
    // widget line 1
    m_voucherNrLabel = new QLabel(tr("VoucherNr:"));
    m_voucherNrValue = new QLabel("0");
    m_voucherNrLabel->setBuddy(m_voucherNrValue);
    
    m_dateLabel = new QLabel(tr("Date:"));
    m_dateValue = new QLabel("01.01.1900"); // later: QDateEdit
    m_dateLabel->setBuddy(m_dateValue);
    
    m_cashierLabel = new QLabel(tr("Cashier:"));
    m_cashierCombo = new QComboBox;
    fillComboFromStringlist(cashierList, m_cashierCombo);
    m_cashierLabel->setBuddy(m_cashierCombo);
    
    
    // widget line 2
    m_purposeLabel = new QLabel(tr("Purpose:"));
    m_purposeCombo = new QComboBox;
    fillComboFromStringlist(purposeList, m_purposeCombo);
    m_purposeLabel->setBuddy(m_purposeCombo);
    
    m_descrLabel = new QLabel(tr("Description:"));
    m_descrEdit = new QLineEdit;
    m_descrLabel->setBuddy(m_descrEdit);
    
    
    // widget line 3 - block left
    m_voucherTypeLabel = new QLabel(tr("Voucher kind:"));  // income/expense
    m_voucherTypeRadioGroup = new QButtonGroup;  // only for mutex logic
    m_voucherTypeIncomeRadio =  new QRadioButton(tr("Income"));
    m_voucherTypeExpenseRadio = new QRadioButton(tr("Expense"));
    m_voucherTypeRadioGroup->addButton(m_voucherTypeIncomeRadio);
    m_voucherTypeRadioGroup->addButton(m_voucherTypeExpenseRadio);
    m_voucherTypeIncomeRadio->setChecked(true); // set income as default
    
    m_amountLabel = new QLabel(tr("Amount:"));
    m_amountEdit = new QLineEdit;
    m_amountEdit->setValidator(new QDoubleValidator(0.0,99999.0,2, m_amountEdit));
    m_amountLabel->setBuddy(m_amountEdit);
    
    
    // widget line 3  - block right
    m_voucherConfirmButton = new QPushButton(tr("Confirm voucher"));
    m_voucherDiscardButton = new QPushButton(tr("Discard voucher"));
    
    
    // group 2
    m_pettycashStateGroup = new QGroupBox(tr("Pettycash status"));
    
    // widget line 1
    m_pettycashName = new QLabel(pettyCashName.c_str());
    
    // widget line 2
    m_numVouchersLabel = new QLabel(tr("Nr. of vouchers stored:"));
    m_numVouchersValue = new QLabel("0");
    m_numVouchersLabel->setBuddy(m_numVouchersValue);
    
    m_cashpositionLabel = new QLabel(tr("Actual cash position"));
    m_cashpositionValue = new QLabel("0.00");
    m_cashpositionLabel->setBuddy(m_cashpositionValue);
    
    m_viewAllVouchersButton = new QPushButton(tr("View all vouchers"));

}



void CVoucherEditDialog::layoutWidgets()
{
    // group 1 - line 1
    m_layoutLineGrp1Line1 = new QHBoxLayout; 
    m_layoutLineGrp1Line1->addWidget(m_voucherNrLabel);
    m_layoutLineGrp1Line1->addWidget(m_voucherNrValue);
    m_layoutLineGrp1Line1->addWidget(m_dateLabel);
    m_layoutLineGrp1Line1->addWidget(m_dateValue);
    m_layoutLineGrp1Line1->addStretch();
    m_layoutLineGrp1Line1->addWidget(m_cashierLabel);
    m_layoutLineGrp1Line1->addWidget(m_cashierCombo);
    
    // group 1 - line 2
    m_layoutLineGrp1Line2 = new QHBoxLayout;
    m_layoutLineGrp1Line2->addWidget(m_purposeLabel);
    m_layoutLineGrp1Line2->addWidget(m_purposeCombo);
    m_layoutLineGrp1Line2->addWidget(m_descrLabel);
    m_layoutLineGrp1Line2->addWidget(m_descrEdit);
    
    
    // group 1 - line 3 - block left
    m_layoutVoucherTypeRadio =  new QVBoxLayout;
    m_layoutVoucherTypeRadio->addWidget(m_voucherTypeIncomeRadio);
    m_layoutVoucherTypeRadio->addWidget(m_voucherTypeExpenseRadio);
  
    m_layoutVoucherType = new QHBoxLayout;
    m_layoutVoucherType->addWidget(m_voucherTypeLabel);
    m_layoutVoucherType->addLayout(m_layoutVoucherTypeRadio);
    m_layoutVoucherType->addStretch();
    
    m_layoutAmount = new QHBoxLayout;
    m_layoutAmount->addWidget(m_amountLabel);
    m_layoutAmount->addWidget(m_amountEdit);
     
    m_layoutBlockLeftGrp1Line3 = new QVBoxLayout;
    m_layoutBlockLeftGrp1Line3->addLayout(m_layoutVoucherType);
    m_layoutBlockLeftGrp1Line3->addLayout(m_layoutAmount);
   
    // group 1 - line 3 - block right
    m_layoutBlockRightGrp1Line3 = new QVBoxLayout;
    m_layoutBlockRightGrp1Line3->addWidget(m_voucherConfirmButton);
    m_layoutBlockRightGrp1Line3->addWidget(m_voucherDiscardButton);
    
    // arrgane group 1 - lin 3
    m_layoutLineGrp1Line3 = new QHBoxLayout;
    m_layoutLineGrp1Line3->addLayout(m_layoutBlockLeftGrp1Line3);
    m_layoutLineGrp1Line3->addStretch();
    m_layoutLineGrp1Line3->addLayout(m_layoutBlockRightGrp1Line3);
    
    
    // arrange group 1
    m_layoutGroup1 = new QVBoxLayout;
    m_layoutGroup1->addLayout(m_layoutLineGrp1Line1);
    m_layoutGroup1->addStretch();
    m_layoutGroup1->addLayout(m_layoutLineGrp1Line2);
    m_layoutGroup1->addStretch();
    m_layoutGroup1->addLayout(m_layoutLineGrp1Line3);
    
    m_voucherInputGroup->setLayout(m_layoutGroup1);
    m_voucherInputGroup->setFlat(false);
    
    
    // group 2 - line 2
    m_layoutLineGrp2Line2 = new QHBoxLayout;
    m_layoutLineGrp2Line2->addWidget(m_numVouchersLabel);
    m_layoutLineGrp2Line2->addWidget(m_numVouchersValue);
    m_layoutLineGrp2Line2->addWidget(m_cashpositionLabel);
    m_layoutLineGrp2Line2->addWidget(m_cashpositionValue);
    m_layoutLineGrp2Line2->addWidget(m_viewAllVouchersButton);
    

    // arrange group 2
    m_layoutGroup2 =  new QVBoxLayout;
    m_layoutGroup2->addWidget(m_pettycashName);
    m_layoutGroup2->addLayout(m_layoutLineGrp2Line2);
    
    m_pettycashStateGroup->setLayout(m_layoutGroup2);
    m_pettycashStateGroup->setFlat(false);
    
    // arrange the whole thing
    m_mainLayout = new  QVBoxLayout;
    m_mainLayout->addWidget(m_voucherInputGroup);
    m_mainLayout->addWidget(m_pettycashStateGroup);
    setLayout(m_mainLayout);
}



void CVoucherEditDialog::setupConnections()
{
    connect(m_voucherConfirmButton, SIGNAL(clicked()),
            this, SLOT(slotCheckVoucher()));   
            
    connect(m_voucherDiscardButton, SIGNAL(clicked()),
            this, SLOT(slotDiscardVoucher()));

    connect(m_purposeCombo, SIGNAL(activated(const QString &)),
            this, SLOT(slotPreselectVouchertype(const QString &)));
            
    connect(m_viewAllVouchersButton,SIGNAL(clicked()),
            this, SIGNAL(signalVoucherListRequested()));            
}



void CVoucherEditDialog::fillComboFromStringlist(const std::list<std::string>& textlist,
                                                 QComboBox* targetCombo)
{
    for (std::list<std::string>::const_iterator stringListIter = textlist.begin();
         stringListIter!= textlist.end();
         ++stringListIter)
    {
        QString actComboEntry((*stringListIter).c_str());
        targetCombo->addItem(actComboEntry);
    }
}


int CVoucherEditDialog::getMessageDialogResult(const SVoucherData& actVoucherData)
{
    QString msgInformationText = tr("Voucher kind: ");
    if (vouchertype_income == actVoucherData.vouchertype)
    {
        msgInformationText += tr("INCOME");
    }
    else
    {
        msgInformationText += tr("EXPENSE");
    }
    msgInformationText += "\n";
    msgInformationText += tr("Amount: ");
    msgInformationText += QString::number(actVoucherData.amount,'f',2);
    msgInformationText += "\n\n";
    msgInformationText += tr("Shall this voucher really be saved?");

    // additional message box to avoid wrong inputs
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(tr("Prompt for confirmation"));
    msgBox.setText(tr("Please confirm the entered voucher"));
    msgBox.setInformativeText(msgInformationText);
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard);
    msgBox.setDefaultButton(QMessageBox::Save);

    return(msgBox.exec());
}
