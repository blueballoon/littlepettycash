/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __DATAMANAGER_HPP__
#define __DATAMANAGER_HPP__

#include <string>
#include <QString>
#include "pettyCashTypes.hpp"
#include <list>

class CDataManager
{
public:
    CDataManager(const std::string& dataFileName);   //constructor

    SStoredDataSummary getBriefStoredData() const;

    std::list<SStoredVoucherData> getDetailedStoredData() const;

    void appendVoucherData(int voucherNr, std::string dateString,
                           const SVoucherData& voucherData);

private: //methods
    ///@brief creates regex for the defined csv line format
    ///@return the regex as qt regex object
    ///@note the regex captions cover all columns and are defined as follows:
    ///      1 - VoucherNr
    ///      2 - Date
    ///      3 - Cashier
    ///      4 - Purpose
    ///      5 - Description
    ///      6 - Income
    ///      7 - Expense
    QRegExp getCsvRegExp() const;


    ///@brief parses on csv line
    bool parseLine(const QString& line, SStoredVoucherData& voucherData) const;


private: //attributes

    const std::string m_dataFileName;
    const char m_separator;
    const char m_replacechar;

};

#endif
