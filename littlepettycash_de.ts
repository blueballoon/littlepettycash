<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE" sourcelanguage="en_US">
<context>
    <name>CPettyCashLogic</name>
    <message>
        <source>Voucher kind: </source>
        <translation type="obsolete">Belegart: </translation>
    </message>
    <message>
        <source>INCOME</source>
        <translation type="obsolete">EINGANG</translation>
    </message>
    <message>
        <source>EXPENSE</source>
        <translation type="obsolete">AUSGANG</translation>
    </message>
    <message>
        <source>Amount: </source>
        <translation type="obsolete">Betrag: </translation>
    </message>
    <message>
        <source>Shall this voucher really be saved?</source>
        <translation type="obsolete">Soll dieser Beleg wirklich gespeichert werden?</translation>
    </message>
    <message>
        <source>Prompt for confirmation</source>
        <translation type="obsolete">Sicherheitsabfrage</translation>
    </message>
    <message>
        <source>Please confirm the entered voucher</source>
        <translation type="obsolete">Bitte den eingegebenen Beleg bestätigen</translation>
    </message>
</context>
<context>
    <name>CVoucherEditDialog</name>
    <message>
        <source>Voucher input</source>
        <translation>Belegeingabe</translation>
    </message>
    <message>
        <source>VoucherNr:</source>
        <translation>Belegnummer:</translation>
    </message>
    <message>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <source>Cashier:</source>
        <translation>Kassier:</translation>
    </message>
    <message>
        <source>Purpose:</source>
        <translation>Zweck:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <source>Voucher kind:</source>
        <translation>Belegart:</translation>
    </message>
    <message>
        <source>Income</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <source>Expense</source>
        <translation>Ausgang</translation>
    </message>
    <message>
        <source>Amount:</source>
        <translation>Betrag:</translation>
    </message>
    <message>
        <source>Confirm voucher</source>
        <translation>Beleg übernehmen</translation>
    </message>
    <message>
        <source>Discard voucher</source>
        <translation>Beleg verwerfen</translation>
    </message>
    <message>
        <source>Pettycash status</source>
        <translation>Kassenstatus</translation>
    </message>
    <message>
        <source>Nr. of vouchers stored:</source>
        <translation>Anzahl gespeicherter Belege:</translation>
    </message>
    <message>
        <source>Actual cash position</source>
        <translation>Aktueller Kassenstand</translation>
    </message>
    <message>
        <source>Voucher kind: </source>
        <translation>Belegart: </translation>
    </message>
    <message>
        <source>INCOME</source>
        <translation>EINGANG</translation>
    </message>
    <message>
        <source>EXPENSE</source>
        <translation>AUSGANG</translation>
    </message>
    <message>
        <source>Amount: </source>
        <translation>Betrag: </translation>
    </message>
    <message>
        <source>Shall this voucher really be saved?</source>
        <translation>Soll dieser Beleg wirklich gespeichert werden?</translation>
    </message>
    <message>
        <source>Prompt for confirmation</source>
        <translation>Sicherheitsabfrage</translation>
    </message>
    <message>
        <source>Please confirm the entered voucher</source>
        <translation>Bitte den eingegebenen Beleg bestätigen</translation>
    </message>
    <message>
        <source>View all vouchers</source>
        <translation>Zeige alle Belege</translation>
    </message>
</context>
<context>
    <name>CVoucherListDialog</name>
    <message>
        <source>VoucherNr.</source>
        <translation>Belegnr.</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Cashier</source>
        <translation>Kassier</translation>
    </message>
    <message>
        <source>Purpose</source>
        <translation>Zweck</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Amount</source>
        <translation>Betrag</translation>
    </message>
    <message>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>List of stored vouchers</source>
        <translation>Liste der gespeicherten Belege</translation>
    </message>
</context>
</TS>
