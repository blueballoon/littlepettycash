/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __VOUCHEREDITDIALOG_HPP__
#define __VOUCHEREDITDIALOG_HPP__

#include <QDialog>
#include <string>
#include <list>
#include <map>
#include "pettyCashTypes.hpp"

// forwards
class QLabel;
class QRadioButton;
class QLineEdit;
class QPushButton;
class QGroupBox;
class QButtonGroup;
class QVBoxLayout;
class QHBoxLayout;
class QComboBox;

// the view


class CVoucherEditDialog : public QDialog
{
	Q_OBJECT
public:
	CVoucherEditDialog(const std::string& pettyCashName,
                           const std::list<std::string>& cashierList,
                           const std::list<std::pair<std::string,EVoucherType> >& purposeList,
                           bool confirmationDialogActive,
                           QWidget *parent = 0);

signals:
	void signalVoucherConfirmed(const SVoucherData&);
	void signalVoucherListRequested();
	
public slots:
    void slotSetVoucherToInputState(const SPettyCashData&);
	
private slots:
    // after pressing confimVoucher, check if all data is valid
    void slotCheckVoucher();  
    void slotDiscardVoucher(); // reset entries if discard is pressed
    void slotPreselectVouchertype(const QString &);  //preselect the voucher type when purpose is choosen
	
private: //methods
    void createWidgets(const std::string &pettyCashName,
                           const std::list<std::string>& cashierList,
                           const std::list<std::string>& purposeList);
    void layoutWidgets();
    void setupConnections();
    
    void fillComboFromStringlist(const std::list<std::string>& textlist,
                                                 QComboBox* targetCombo);

    int getMessageDialogResult(const SVoucherData&);
    
private: // attributes
    // group 1
    QGroupBox* m_voucherInputGroup;
    QVBoxLayout* m_layoutGroup1;
   
    // widget line 1
    QLabel*    m_voucherNrLabel;
    QLabel*    m_voucherNrValue;
    QLabel*    m_dateLabel;
    QLabel*    m_dateValue;
    QLabel*    m_cashierLabel;
    // QLineEdit* m_cashierEdit;
    QComboBox* m_cashierCombo;
    QHBoxLayout* m_layoutLineGrp1Line1;
    
    // widget line 2
    QLabel*    m_purposeLabel;
    // QLineEdit* m_purposeEdit;
    QComboBox* m_purposeCombo;
    QLabel*    m_descrLabel;
    QLineEdit* m_descrEdit;
    QHBoxLayout* m_layoutLineGrp1Line2;
    
    // widget line 3 - block left
    QLabel*       m_voucherTypeLabel;
    QButtonGroup* m_voucherTypeRadioGroup;
    QRadioButton* m_voucherTypeIncomeRadio;
    QRadioButton* m_voucherTypeExpenseRadio;
    QVBoxLayout*  m_layoutVoucherTypeRadio;
    QHBoxLayout*  m_layoutVoucherType;
    
    QLabel*       m_amountLabel;
    QLineEdit*    m_amountEdit;
    QHBoxLayout*  m_layoutAmount;
    QVBoxLayout*  m_layoutBlockLeftGrp1Line3;
    
    // widget line 3  - block right
    QPushButton* m_voucherConfirmButton;
    QPushButton* m_voucherDiscardButton;
    QVBoxLayout*  m_layoutBlockRightGrp1Line3;
    
    QHBoxLayout* m_layoutLineGrp1Line3;
    
    // group 2
    QGroupBox* m_pettycashStateGroup;
    QVBoxLayout* m_layoutGroup2;
    
    // widget line 1
    QLabel* m_pettycashName;
    
    // widget line 2
    QLabel* m_numVouchersLabel;
    QLabel* m_numVouchersValue;
    QLabel* m_cashpositionLabel;
    QLabel* m_cashpositionValue;
    QPushButton* m_viewAllVouchersButton;
    QHBoxLayout* m_layoutLineGrp2Line2;
    
    // the whole thing
    QVBoxLayout* m_mainLayout;

private: // connection logic attributes
    std::map<std::string, EVoucherType> m_purposeTypeMap;
    // flag to indicate if additional confirmation is needed before storing the voucher
    bool m_confirmationDialogActive;
};


#endif
