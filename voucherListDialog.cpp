/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtGui>
#include <QMessageBox>
#include <string>
#include "voucherListDialog.hpp"
//debug only #include <iostream>

// constructor
CVoucherListDialog::CVoucherListDialog(const std::list<SStoredVoucherData>& storedVoucherData,
		QWidget *parent)
{
	const int nrColumns = 6; // currently, we have a fixed number of 6 columns
	const int nrRows = storedVoucherData.size();
	m_voucherTable = new QTableWidget(nrRows, nrColumns);


	QStringList headerStringList;
	headerStringList << tr("VoucherNr.");
	headerStringList << tr("Date");
	headerStringList << tr("Cashier");
	headerStringList << tr("Purpose");
	headerStringList << tr("Description");
	headerStringList << tr("Amount");
	m_voucherTable->setHorizontalHeaderLabels(headerStringList);


    int currVoucherListRow = 0;
    for(std::list<SStoredVoucherData>::const_iterator
    		voucherIter = storedVoucherData.begin();
    		voucherIter != storedVoucherData.end();
    		++voucherIter)
    {
    	SStoredVoucherData currVoucherData = *voucherIter;

  		QTableWidgetItem *voucherNrItem = new QTableWidgetItem(
  				tr("%1").arg(currVoucherData.voucherNr));
  		voucherNrItem->setFlags(Qt::ItemIsEnabled);
  		m_voucherTable->setItem(currVoucherListRow, 0, voucherNrItem);

  		QTableWidgetItem *voucherDateItem = new QTableWidgetItem(
  				static_cast<QString>(currVoucherData.voucherDate.c_str()));
  		voucherDateItem->setFlags(Qt::ItemIsEnabled);
  		m_voucherTable->setItem(currVoucherListRow, 1, voucherDateItem);

  		QTableWidgetItem *voucherCashierItem = new QTableWidgetItem(
  				static_cast<QString>(currVoucherData.cashier.c_str()));
  		voucherCashierItem->setFlags(Qt::ItemIsEnabled);
  		m_voucherTable->setItem(currVoucherListRow, 2, voucherCashierItem);

  		QTableWidgetItem *voucherPurposeItem = new QTableWidgetItem(
  				static_cast<QString>(currVoucherData.purpose.c_str()));
  		voucherPurposeItem->setFlags(Qt::ItemIsEnabled);
  		if (currVoucherData.amount < 0.0)
  		{
  			// display expense-purposes as red
  			voucherPurposeItem->setForeground(QColor::fromRgb(255,0,0));
  		}
  		m_voucherTable->setItem(currVoucherListRow, 3, voucherPurposeItem);

  		QTableWidgetItem *voucherDescItem = new QTableWidgetItem(
  				static_cast<QString>(currVoucherData.description.c_str()));
  		voucherDescItem->setFlags(Qt::ItemIsEnabled);
  		m_voucherTable->setItem(currVoucherListRow, 4, voucherDescItem);

  		QTableWidgetItem *voucherAmountItem = new QTableWidgetItem(
  				tr("%1").arg(currVoucherData.amount,5,'f',2));
  		voucherAmountItem->setFlags(Qt::ItemIsEnabled);
  		voucherAmountItem->setTextAlignment(Qt::AlignRight);
  		if (currVoucherData.amount < 0.0)
  		{
  			// display expenses as red
  		    voucherAmountItem->setForeground(QColor::fromRgb(255,0,0));
  		}
  		m_voucherTable->setItem(currVoucherListRow, 5, voucherAmountItem);

    	++currVoucherListRow;
    }

    for (int currVoucherListCol = 0; currVoucherListCol < nrColumns; ++currVoucherListCol)
    {
    	m_voucherTable->resizeColumnToContents (currVoucherListCol);
    }

    int voucherTableWidth = m_voucherTable->width();


	QVBoxLayout* voucherListLayout =  new QVBoxLayout;
	voucherListLayout->addWidget(m_voucherTable);
	setLayout(voucherListLayout);
	int currDialogHeight=this->height();
	// factor 1.2 seems to fit the real size!?
	this->resize(voucherTableWidth*1.2,currDialogHeight);
}
