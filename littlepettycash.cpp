/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QLabel>
#include <QTranslator>
#include <QString>

#include <string>

#include "voucherEditDialog.hpp"
#include "voucherListDialog.hpp"
#include "pettyCashLogic.hpp"
#include "cfgManager.hpp"


//debug only
#include <iostream>

int main(int argc, char* argv[])
{
    // if arg passed, its the name of the config file
    std::string configName = "littlepettycash.cfg";
    if (argc >= 2)  // first arg is executeable, second is first "real" argument
    {
        configName=argv[1];
    }

    // read and parse config, or provide internal default config
    CCfgManager*  cfgManager = new CCfgManager(configName);


    QApplication lpcApp(argc, argv);
    lpcApp.setStyle("cleanlooks");


    // set language
    // currently, only de is supported as translation
    // otherwise, builtin english is used
    QTranslator lpcTranslator;
    if ("de" == cfgManager->getLanguage())
    {
        QString translationFile("littlepettycash_de.qm");
        QString translationFilePath(":");
        bool translatorOk = lpcTranslator.load(translationFile,translationFilePath);
        if (translatorOk)
        {
            lpcApp.installTranslator(&lpcTranslator);
        }
    }

    // model/data
    CDataManager* dataManager = new CDataManager(cfgManager->getCSVdatabaseName());

    // view(s)
    CVoucherEditDialog* voucherEditDialog =
            new CVoucherEditDialog(cfgManager->getPettycashname(),
                                   cfgManager->getCashierList(),
                                   cfgManager->getPurposeList(),
                                   cfgManager->isConfirmationDialogActive());



    // controller - fix connected to data model
    CPettyCashLogic* pettyCashLogic = new CPettyCashLogic(dataManager);


    // observer connections
    QObject::connect(voucherEditDialog, SIGNAL(signalVoucherConfirmed(const SVoucherData&)),
                     pettyCashLogic, SLOT(slotActVoucher(const SVoucherData&)));

    QObject::connect(pettyCashLogic, SIGNAL(signalVoucherStored(const SPettyCashData&)),
                     voucherEditDialog, SLOT(slotSetVoucherToInputState(const SPettyCashData&)) );

    QObject::connect(voucherEditDialog, SIGNAL(signalVoucherListRequested()),
                     pettyCashLogic, SLOT(slotAllVoucherView()));


    // run program
    pettyCashLogic->doInitialisation();
    voucherEditDialog->setWindowTitle("Little Pettycash v0.4");



    voucherEditDialog->show();
	return(lpcApp.exec());


    //cleanup
    delete pettyCashLogic;
    delete voucherEditDialog;
    delete dataManager;
    delete cfgManager;
}
