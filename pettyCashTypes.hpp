/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __PETTYCASHTYPES_HPP__
#define __PETTYCASHTYPES_HPP__

enum EVoucherType 
{
    vouchertype_income,
    vouchertype_expense
};


struct SVoucherData
{
    std::string cashier;
    std::string purpose;
    std::string description;
    EVoucherType vouchertype;
    float amount;
};

struct SPettyCashData
{
    int nextVoucherNr; // = numStoredVouchers+1
    int numStoredVouchers;  
    float cashposition;
    std::string dateString;
};

struct SStoredDataSummary
{
    int numStoredVouchers;  
    float cashposition;    
};

// currently, needed for viewing list of vouchers
struct SStoredVoucherData
{
	int voucherNr;
	std::string voucherDate;
	std::string cashier;
	std::string purpose;
	std::string description;
	float amount;   // positive - income; negative - expense
};

#endif
