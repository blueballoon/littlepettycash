/*
   Copyright 2012 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QFile>
#include <QDomDocument>
#include <utility>

#include "cfgManager.hpp"

//debug only
#include <iostream>

CCfgManager::CCfgManager(const std::string& configFileName)
{
//    std::cerr << "configfilename =  <" << configFileName << ">" << std::endl;

    // set default settings
    m_pettycashname = "just another petty cash";
    m_CSVdatabaseName = "littlepettycash.csv";
    m_language = "en";
    m_confirmationDialogActive = false;



    if (!readAndParseConfigfile(configFileName))
    {
        // set the default values for cashier and purpose that shall be returned
        // if config file cannot be parsed
        m_cashierList.push_back("John Doe");
        m_cashierList.push_back("Config Maker");
        m_purposeList.push_back(std::make_pair("ACME fee",vouchertype_expense));
        m_purposeList.push_back(std::make_pair("make own config",vouchertype_income));


        ///@todo additional error handling (eg warning dialog)
    }
}



bool CCfgManager::readAndParseConfigfile(const std::string& configFileName)
{
    bool parsingOk = false;

    // open file
    QFile configQFile(configFileName.c_str());
    if(!configQFile.open(QFile::ReadOnly | QFile::Text))
    {
        // todo error handling
//        std::cerr << "readcfg: cannot open file" << std::endl;
        return(parsingOk); //false
    }

    // parse and build dom
    QString domErrorString;
    int errorLine;
    int errorColumn;
    QDomDocument configDomDoc;
    if (!configDomDoc.setContent(&configQFile,
                              false, // no namespace processing
                              &domErrorString,
                              &errorLine, &errorColumn))
    {
        // todo error handling
 //       std::cerr << "readcfg: parse error" <<
 //               domErrorString.toStdString() << " in line " <<
 //               errorLine << ", column " << errorColumn << std::endl;
        return(parsingOk); //false
    }

    // traverse thru dom and extract the configuration
    QDomElement rootElement = configDomDoc.documentElement();
    if (rootElement.tagName() != "littlepettycashcfg")
    {
        // todo error handling
//        std::cerr << "readcfg: not a littlepettycash config file" << std::endl;
        return(parsingOk); //false
    }

    parsingOk = parseLittlepettycashconfigElement(rootElement);

//    if (parsingOk)
//        std::cerr << "parsing ok" << std::endl;
//    else
//        std::cerr << "parsing NOT ok" << std::endl;
    
    return(parsingOk);
}



bool CCfgManager::parseLittlepettycashconfigElement(const QDomElement& element)
{
    bool parsingOk=true;

    QDomNode child = element.firstChild();
    while (!child.isNull() && parsingOk)
    {
        if (child.toElement().tagName() == "settings")
        {
            parsingOk = parseSettingsElement(child.toElement());
        }
        else if (child.toElement().tagName() == "cashierlist")
        {
            parsingOk = parseCashierlistElement(child.toElement());
        }
        else if (child.toElement().tagName() == "purposelist")
        {
            parsingOk = parsePurposelistElement(child.toElement());
        }
        // ignor all other toplevel tags

        child = child.nextSibling();
    }

    return(parsingOk);
}



bool CCfgManager::parseSettingsElement(const QDomElement& element)
{
    bool parsingOk=true;

    QDomNode child = element.firstChild();
    while (!child.isNull() && parsingOk)
    {
        if (child.toElement().tagName() == "pettycashname")
        {
            QString  pettycashname = child.toElement().text();
            m_pettycashname = pettycashname.toStdString();
//            std::cerr << "pettycashname: <" << m_pettycashname << ">" << std::endl;
        }
        else if (child.toElement().tagName() == "csvdatabase")
        {
            QString  csvdatabase = child.toElement().text();
            m_CSVdatabaseName = csvdatabase.toStdString();
//            std::cerr << "csvdatabase: <" << m_CSVdatabaseName << ">" << std::endl;
        }
        else if (child.toElement().tagName() == "language")
        {
            QString  language = child.toElement().text();
            m_language = language.toStdString();
//            std::cerr << "language: <" << m_language << ">" << std::endl;
        }
        else if (child.toElement().tagName() == "confirmationdialog")
        {
            QString  confirmationdialog = child.toElement().text();
            if ("yes" == confirmationdialog)
            {
                m_confirmationDialogActive = true;
            }
            else
            {
                m_confirmationDialogActive = false;
            }
//            std::cerr << "confirmationdialog: <" << m_confirmationDialogActive << ">" << std::endl;
        }
        // ignor all other settings tags

        child = child.nextSibling();
    }

    // currently, it cannot fail, because default values are used for all
    // elements not defined in config file
    return(parsingOk);
}


// todo refctoring: make one "parseAnylistElemnt" method that get target list
// and tag name as parameter => replaces parseCashierlistElement and
// parseCashierlistElement
bool CCfgManager::parseCashierlistElement(const QDomElement& element)
{
    bool parsingOk=true;

    //remove default (dummy) cashiers
    m_cashierList.clear();

    QDomNode child = element.firstChild();
    while (!child.isNull() && parsingOk)
    {
        if (child.toElement().tagName() == "cashier")
        {
            QString  cashier = child.toElement().text();
            m_cashierList.push_back(cashier.toStdString());
 //           std::cerr << "cashier: <" << cashier.toStdString() << ">" << std::endl;
        }
        // ignore all other cashierlist tags

        child = child.nextSibling();
    }

    // check if at least one cashier have been read
    // otherwise, a serious config error occured
    if (m_cashierList.empty())
    {
        parsingOk = false;
    }

    return(parsingOk);
}



bool CCfgManager::parsePurposelistElement(const QDomElement& element)
{
    bool parsingOk=true;

    //remove default (dummy) cashiers
    m_purposeList.clear();

    QDomNode child = element.firstChild();
    while (!child.isNull() && parsingOk)
    {
        if (child.toElement().tagName() == "purpose")
        {
	    QString purposeType = child.toElement().attribute("type");	    
            QString  purpose = child.toElement().text();
            std::pair<std::string,EVoucherType> actPurpose;
            actPurpose.first = purpose.toStdString();
            if (purposeType == "expense")
            {
                 actPurpose.second = vouchertype_expense;
            }
            else  // per default, set voucher to income
            {
                actPurpose.second = vouchertype_income;
            }

            m_purposeList.push_back(actPurpose);
            // std::cerr << "purpose: <" << purpose.toStdString() << ">, type: <" << purposeType.toStdString() << ">" << std::endl;
        }
        // ignor all other purposelist tags

        child = child.nextSibling();
    }

    // check if at least one purpose have been read
    // otherwise, a serious config error occured
    if (m_purposeList.empty())
    {
        parsingOk = false;
    }

    return(parsingOk);
}

