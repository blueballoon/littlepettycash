/*
   Copyright 2013 Alexander Haas
   This file is part of LittlePettycash.

    LittlePettycash is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LittlePettycash is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LittlePettycash.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string>
#include <iostream>
#include <QDate>
#include "pettyCashLogic.hpp"
#include "voucherListDialog.hpp"

//debug only #include <iostream>

CPettyCashLogic::CPettyCashLogic(CDataManager* dataManager):
        m_dataManager(dataManager)
{
    QDate actDate = QDate::currentDate();
    QString dateQString = actDate.toString("dd.MM.yyyy");
    m_voucherDateString = dateQString.toStdString();
}


void CPettyCashLogic::doInitialisation()
{
    SPettyCashData initPettycashData = getFormattedDataFromStorage();
    emit signalVoucherStored(initPettycashData);
}



void CPettyCashLogic::slotActVoucher(const SVoucherData& actVoucherData)
{
    // request act data from voucher storage to determine the voucher nr
    SStoredDataSummary actStoredData = m_dataManager->getBriefStoredData();
    int actVoucherNr = actStoredData.numStoredVouchers + 1;

    // persist new data
    m_dataManager->appendVoucherData(actVoucherNr, m_voucherDateString,
                                     actVoucherData);

    // get actualised data from storage and return it to gui
    SPettyCashData actPettycashData = getFormattedDataFromStorage();
    emit signalVoucherStored(actPettycashData);
}



void CPettyCashLogic::slotAllVoucherView()
{
	// request all data from voucher storage
	std::list<SStoredVoucherData> actStoredFullData =
			m_dataManager->getDetailedStoredData();

	// logic knows the datamanager, so the voucher view is controlled from here
	CVoucherListDialog* voucherListDialog = new CVoucherListDialog(actStoredFullData);
	voucherListDialog->setWindowTitle(QObject::tr("List of stored vouchers"));
	voucherListDialog->exec(); // see http://stackoverflow.com/a/23225591
	delete voucherListDialog;
}


SPettyCashData CPettyCashLogic::getFormattedDataFromStorage()
{
    // get current data for datamanager
    SStoredDataSummary actStoredData = m_dataManager->getBriefStoredData();
    SPettyCashData actPettycashData;

    actPettycashData.numStoredVouchers = actStoredData.numStoredVouchers;
    actPettycashData.nextVoucherNr = actStoredData.numStoredVouchers + 1;
    actPettycashData.cashposition = actStoredData.cashposition;

    // fill also the correct data although it does not come from storage
    actPettycashData.dateString =  m_voucherDateString;
    return(actPettycashData);
}


